<?php
   $title = "VLC media player for Ubuntu";
   $new_design = true;
   $lang = "en";
   $menu = array( "vlc", "download" );

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/style/panels.css");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
   include($_SERVER["DOCUMENT_ROOT"]."/include/os-specific.php");
   include($_SERVER["DOCUMENT_ROOT"]."/include/package.php");
?>

<div class="container">
	<?php
	$screenshots = getScreenshots("linux");
	$defaultDetail = getOS("linux");
	?>
    <section class="download-wrapper">
        <div class="row reorder-xs">
            <?php
                drawScreenshots($screenshots);
            ?>
            <div class="v-align col-sm-5">
                <div class="center-font-xs">
                    <?php image('largeVLC.png', 'Large Orange VLC media player Traffic Cone Logo', 'big-vlc-img img-responsive visible-xs-inline-block v-align'); ?>
                    <h1 class="v-align bigtitle">
                        VLC media player for <a href="http://www.ubuntu.com/">Ubuntu</a>
                    </h1>
                </div>
                <div class="projectDescription hidden-sm hidden-xs">
                    <?php echo
                    _("VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols."); ?>
                </div>
                <div class="projectDescription visible-xs visible-sm center-font-xs">
                    <?php echo
                    _("VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols."); ?>
                </div>

			    <div class="inner center-xs">
			        <div class="btn-group">
                            <a href="snap://vlc"><img src="//images.videolan.org/images/badges/[EN]-snap-store-white.png" title="Download for Ubuntu" alt="Download for Ubuntu button" /></a>
			        </div>
			        <div id="downloadDetails">
			            <span id='downloadOS'><?php echo $defaultDetail["name"]; ?></span>
			        </div>
			    </div>
            </div>
        </div>
    </section>

<h2>Supported releases</h2>
<p class="projectDescription">Ubuntu 18.04 <span style='font-size: smaller'>&ldquo;Bionic Beaver&rdquo;</span><br />
Ubuntu 16.04 <span style='font-size: smaller'>&ldquo;Xenial Xerus&rdquo;</span><br />
</p>

<h2>Installation the Graphical way</h2>
<p class="projectDescription"><p>Open <b>Ubuntu Software</b> application.
</p>

<p class="projectDescription">Search for <kbd>vlc</kbd> and install it.

<h2>Installation the Command line way</h2>
<p><blockquote>
<pre>
% sudo snap install vlc
</pre>
</blockquote>
</p>

<h3>Nota Bene</h3>
<p class="projectDescription">VLC for Ubuntu and many other Linux distributions is packaged using <a href="https://snapcraft.io/">snapcraft</a>.
This allows us to distribute latest and greatest VLC versions directly to end
users, with security and critical bug fixes, full codec and optical media
support.  <br />
If you wish to install the traditional deb package, it is available as usual
via <a href="apt://vlc">APT</a>, with all security and critical bug fixes.
However, there will be no major VLC version updates until the next Ubuntu
release.  </p>

<div id="right">
<?php panel_start( "orange" ); ?>
<h1>Playing DVD (libdvdcss)</h1>
<p>In order to be able to play region-locked DVD, you need install libdvdcss2.
You should follow this <a href="https://help.ubuntu.com/community/RestrictedFormats/PlayingDVDs">procedure</a>
to install libdvdcss2.  This applies to deb packages only.</p>
<?php panel_end(); ?>
</div>

</div>

<?php
  footer('$Id$');
?>

